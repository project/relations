
Drupal.relationsTriggerAHAH = function(link) {
  $('#' + link.id).attr('disabled', true);
  if (!$('#' + link.id + '.ahah-processed').size()) {
    $('#' + link.id).bind('click', function() { return false; });
    var ahah = new Drupal.ahah(link.id, {
      'url':      link.href,
      'event':    'click',
      'keypress': true,
      'wrapper':  'relations-table',
      'selector': '#' + link.id,
      'effect':   'fade',
      'method':   'replace',
      'progress': { 'type': 'throbber' },
      'button':   { 'op': link.innerText },
      'element':  link
    });
    $('#' + link.id).addClass('ahah-processed');
    setTimeout(function() { $('#' + link.id).trigger('click'); }, 100);
  }
  return false;
};
