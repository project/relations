<?php

//////////////////////////////////////////////////////////////////////////////
// Node relationship tab

function relations_node_title_autocomplete($node1 = TRUE, $string = '') {
  $matches = array();
  if (!empty($string)) {
    $result = is_object($node1) ?
      db_query("SELECT nid, title FROM {node} WHERE nid != %d AND LOWER(title) LIKE LOWER('%s%%') ORDER BY title ASC", $node1->nid, $string) :
      db_query("SELECT nid, title FROM {node} WHERE LOWER(title) LIKE LOWER('%s%%') ORDER BY title ASC", $string);
    while (($node2 = db_fetch_object($result)) && count($matches) <= 15) {
      // TODO: filter out already related nodes.

      // Check whether the current user's access privileges permit the
      // creation of this relationship:
      if (relations_allowed($node1, $node2 = node_load($node2->nid))) {

        // To prevent ambiguous behavior in cases where multiple nodes have
        // identical titles, we return the node's full URL as the key:
        $url = url($path = drupal_get_path_alias('node/' . $node2->nid), array('absolute' => TRUE));
        $matches[$url] = check_plain($node2->title);
      }
    }
  }
  drupal_json($matches);
}

function relations_node_title_lookup($title) {
  return node_load(array('title' => $title));
}

function relations_node_form($form_state, $node, $is_embedded = FALSE) {
  $can_manage = user_access('create node relations') || user_access('delete node relations');
  return array(
    '#prefix'   => '<div class="form-item" id="relations-table">', // class="clear-block"
    '#suffix'   => $can_manage ? '<div class="description">' . relations_help('node/%/relations#table') . '</div></div>' : '</div>',
    '#theme'    => 'relations_node_form',
    '#tree'     => TRUE,

    'title'     => array(
      '#type'   => 'textfield',
      '#size'   => 32,
      '#maxlength' => 4096,
      '#autocomplete_path' => module_exists('picker') ? '' : 'node/' . (!empty($node->nid) ? $node->nid : 'add') . '/relations/autocomplete',
    ),
    'pick'      => array(
      '#type'   => 'markup',
      '#value'  => !module_exists('picker') ? '' : picker_build_link(array(
        'element'         => $is_embedded ? 'edit-relations-table-title' : 'edit-title',
        'name'            => t('Select content'),
        'content_type'    => relations_get_node_types(),
        'return'          => 'fullnodeurl',
        'seperator'       => ' ',
        'type'            => 'button',
        'view_fields'     => array('title' => t('Title'), 'type' => t('Type'), 'name' => t('Author')),
        'access_callback' => 'relations_picker_access', // @see relations.module
      )),
    ),
    'create'    => !$is_embedded ? array('#type' => 'submit', '#value' => t('Add relationship')) : array(
      '#type'   => 'submit',
      '#value'  => t('Add relationship'),
      '#weight' => 1,
      '#ahah'   => array(
        'path'    => 'node/add/relations/create',
        'wrapper' => 'relations-table',
        'method'  => 'replace',
        'effect'  => 'fade',
      ),
      '#description' => t(''),
    ),
    'nid'       => array(
      '#type'   => 'hidden',
      '#value'  => !empty($node->nid) ? $node->nid : '',
    ),
    'embedded'  => array(
      '#type'   => 'hidden',
      '#value'  => (string)$is_embedded,
    ),
  );
}

function relations_node_form_validate($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  if (empty($title)) {
    return; // when editing a node and using the embedded relationships form
  }
  else if (preg_match('!^(http|https)://!', $title)) {
    //$title = preg_replace('!https://!', '!http://!', $title);
    foreach (explode(' ', trim($title)) as $url) {
      if (!valid_url($url, TRUE)) {
        form_set_error('title', t('Not a valid URL: %url.', array('%url' => $url)));
      }

      if (strpos($url, $GLOBALS['base_url']) !== 0) { // the URL should begin with $base_url
        form_set_error('title', t('Not a valid local URL: %url.', array('%url' => $url)));
      }

      $path = drupal_get_normal_path(substr($url, strlen($GLOBALS['base_url']) + 1));
      if (!preg_match('!node/(\d+)!', $path, $matches) || !($node2 = node_load((int)$matches[1]))) {
        form_set_error('title', t('Not a valid content URL: %url.', array('%url' => $path)));
      }
    }
  }
  else if (preg_match('/^(\d+,?)+$/', $title, $matches)) { // (deprecated) special case for Picker module
    $access = node_access('update', node_load($nid));
    foreach (array_map('intval', explode(',', $title)) as $nid2) {
      if (!$access && !node_access('update', node_load($nid2))) {
        form_set_error('title', t('Your current access privileges prohibit the creation of this relationship.'));
      }
    }
  }
  else if (!($node2 = relations_node_title_lookup($title))) {
    form_set_error('title', t('No matching content titled %title found.', array('%title' => $title)));
  }
  else if (!node_access('update', node_load($nid)) && !node_access('update', $node2)) {
    form_set_error('title', t('Your current access privileges prohibit the creation of this relationship.'));
  }
}

function relations_node_form_submit($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  $nodes = array();
  if (empty($title)) {
    return; // when editing a node and using the embedded relationships form
  }
  else if (preg_match('!^(http|https)://!', $title)) {
    foreach (explode(' ', trim($title)) as $url) {
      $path = drupal_get_normal_path(substr($url, strlen($GLOBALS['base_url']) + 1));
      if (preg_match('!node/(\d+)!', $path, $matches)) {
        $nodes[] = node_load((int)$matches[1]);
      }
    }
  }
  else if (preg_match('/^(\d+,?)+$/', $title, $matches)) { // (deprecated) special case for Picker module
    $nodes = array_map('node_load', array_map('intval', explode(',', $title)));
  }
  else {
    $nodes[] = relations_node_title_lookup($title);
  }

  if (!empty($nodes)) {
    foreach ($nodes as $node2) {
      relations_api_create($nid, $node2->nid);

      watchdog('relations', 'Created relationship from node #%nid1 to #%nid2.', array('%nid1' => $nid, '%nid2' => $node2->nid), WATCHDOG_NOTICE, l(t('view'), 'node/' . $nid . '/relations'));
      drupal_set_message(t('The relationship to <a href="@url">%title</a> was created.', array('%title' => $node2->title, '@url' => url('node/'. $node2->nid))));
    }

    $form_state['redirect'] = 'node/' . $nid . '/relations';
  }
}

function relations_node_delete($form_state, $node1, $node2) {
  $form['nid1'] = array('#type' => 'value', '#value' => $node1->nid);
  $form['nid2'] = array('#type' => 'value', '#value' => $node2->nid);

  return confirm_form($form,
    t('Are you sure you want to delete the relationship to <a href="@url">%title</a>?', array('%title' => $node2->title, '@url' => url('node/'. $node2->nid))),
    isset($_GET['destination']) ? $_GET['destination'] : 'node/'. $node1->nid .'/relations',
    t('This action cannot be undone.'));
}

function relations_node_delete_submit($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);

  if ($form_state['values']['confirm']) {
    relations_api_delete($nid1, $nid2);

    watchdog('relations', 'Deleted relationship from node #%nid1 to #%nid2.', array('%nid1' => $nid1, '%nid2' => $nid2), WATCHDOG_NOTICE, l(t('view'), 'node/'. $nid1 .'/relations'));
    $node2 = node_load($nid2);
    drupal_set_message(t('The relationship to <a href="@url">%title</a> has been deleted.', array('%title' => ($node2 ? $node2->title : '#'. $nid2), '@url' => url('node/'. $nid2))));
  }

  $form_state['redirect'] = 'node/'. $nid1 .'/relations';
}

function relations_node_session_create() {
  $node1 = node_load($_POST['relations_table']['nid']);
  $title = $_POST['relations_table']['title'];

  if (preg_match('/^(\d+,?)+$/', $title, $matches)) { // special case for Picker module
    foreach (array_map('intval', explode(',', $title)) as $nid2) {
      if (($node2 = node_load($nid2))) {
        $_SESSION['relations']['create'][$nid2] = $node->title;
      }
    }
  }
  else if (preg_match('!^(http|https)://!', $title)) {
    foreach (explode(' ', trim($title)) as $url) {
      $path = drupal_get_normal_path(substr($url, strlen($GLOBALS['base_url']) + 1));
      if (preg_match('!node/(\d+)!', $path, $matches)) {
        if (($node2 = node_load($nid2 = (int)$matches[1]))) {
          $_SESSION['relations']['create'][$nid2] = $node->title;
        }
      }
    }
  }
  else if (($node2 = relations_node_title_lookup($title))) {
    $_SESSION['relations']['create'][$node2->nid] = $title;
  }

  $form_state = array('submitted' => FALSE);
  $form = array('#post' => $_POST, '#programmed' => FALSE, '#tree' => FALSE, '#parents' => array());
  $form['relations']['relations_table'] = relations_node_form($form_state, $node1, TRUE);
  $form = form_builder('relations_node_form', $form, $form_state);
  die(drupal_to_js(array('status' => TRUE, 'data' => theme('status_messages') . drupal_render($form))));
}

function relations_node_session_delete($node2) {
  $node1 = node_load($_POST['relations_table']['nid']);
  if (isset($_SESSION['relations']['create'][$node2->nid])) {
    unset($_SESSION['relations']['create'][$node2->nid]);
  }
  else {
    $_SESSION['relations']['delete'][$node2->nid] = $node2->title;
  }

  $form_state = array('submitted' => FALSE);
  $form = array('#post' => $_POST, '#programmed' => FALSE, '#tree' => FALSE, '#parents' => array());
  $form['relations']['relations_table'] = relations_node_form($form_state, $node1, TRUE);
  $form = form_builder('relations_node_form', $form, $form_state);
  die(drupal_to_js(array('status' => TRUE, 'data' => theme('status_messages') . drupal_render($form))));
}

//////////////////////////////////////////////////////////////////////////////
// Node theming

function theme_relations_node_form($form) {
  drupal_add_css(drupal_get_path('module', 'relations') . '/relations.css', 'module', 'all', FALSE);
  drupal_add_js(drupal_get_path('module', 'relations') . '/relations.js');

  return theme('relations_node_table', $form, FALSE);
}

function theme_relations_node_table($form) {
  global $user;
  $node_types  = node_get_types('names');
  $is_new_node = empty($form['nid']['#value']);
  $is_embedded = !empty($form['embedded']['#value']);

  $head = array(t('Title'), t('Type'), t('Author'), array('data' => t('Operations'), 'colspan' => 1));
  $rows = array();

  // Output relationships stored in the database
  if (!$is_new_node) {
    $options = array('user' => $user);

    // Support a ?type=TYPE query argument for filtering the relations tab by content type:
    if (!empty($_GET['type'])) {
      $options['type'] = explode(',', $_GET['type']);
    }

    $node = node_load($form['nid']['#value']);
    foreach (relations_api_lookup($node->nid, $options) as $related) {
      // Hide any previously-stored relationships that have been marked as
      // deleted in a session when editing a node:
      if (!empty($_SESSION['relations']['delete']) && isset($_SESSION['relations']['delete'][$related->nid])) {
        continue;
      }

      $can_delete = user_access('delete node relations') && (node_access('update', $node) || node_access('update', $related));
      $rows[] = array(
        l($related->title, 'node/' . $related->nid),
        isset($node_types[$related->type]) ? t($node_types[$related->type]) : $related->type,
        theme('username', $related),
        !$can_delete ? '' : theme_relations_delete_link($node->nid, $related->nid, $is_embedded),
      );
    }
  }

  // Output relationships stored in the session (when editing a node)
  if (!empty($_SESSION['relations']['create'])) {
    foreach ($_SESSION['relations']['create'] as $nid => $title) {
      if (($related = node_load($nid))) {
        $rows[] = array(
          l($related->title, 'node/' . $related->nid, array('attributes' => array('target' => '_blank'))),
          isset($node_types[$related->type]) ? $node_types[$related->type] : $related->type,
          theme('username', $related),
          theme_relations_delete_link(NULL, $related->nid, $is_embedded),
        );
      }
    }
  }

  // Output the "Add relationship" textbox and button
  if (user_access('create node relations')) {
    $rows[] = array(
      drupal_render($form['title']),
      '',
      drupal_render($form['pick']),
      array('data' => drupal_render($form['create']), 'colspan' => 1),
    );
  }
  else {
    unset($form['title'], $form['create']);
  }

  return drupal_render($form) . theme('table', $head, $rows, array('class' => 'relations'));
}

function theme_relations_delete_link($nid1, $nid2, $is_embedded = TRUE) {
  $path = 'node/' . (!$is_embedded ? $nid1 : 'add') . '/relations/delete/' . $nid2;
  return l(t('delete relationship'), $path, !$is_embedded ? array() : array(
    'attributes' => array(
      'id'      => 'edit-relations-delete-' . $nid2,
      'onclick' => 'return Drupal.relationsTriggerAHAH(this);',
    ),
  ));
}

//////////////////////////////////////////////////////////////////////////////
// Block theming

function theme_relations_node_block($node, array $options = array()) {
  global $user;
  drupal_add_css(drupal_get_path('module', 'relations') .'/relations.css', 'module', 'all', FALSE);

  return theme('relations_node_list', relations_api_lookup($node->nid, array_merge(array('user' => $user), $options)));
}

function theme_relations_node_list($nodes) {
  if (!empty($nodes)) {
    $output = '<div class="item-list">';
    $output .= '<ul>';
    foreach ($nodes as $node) {
      $output .= '<li>'. l($node->title, 'node/'. $node->nid) .'</li>';
    }
    $output .= '</ul>';
    $output .= '</div>';
    return $output;
  }
}
