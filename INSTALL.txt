
REQUIREMENTS
------------
This module requires Drupal 6.x and PHP 5.2.x (or newer), and the following
additional modules:

  * RDF API <http://drupal.org/project/rdf>


INSTALLATION
------------

  1. Copy all the module files into a subdirectory called
     sites/all/modules/relations/ under your Drupal installation directory.

  2. Go to [Administer >> Site building >> Modules] and enable the Relations
     API module. You will find it in the section labelled "RDF".

  3. Go to [Administer >> Site configuration >> RDF settings >> Relations]
     to review and change the configuration options to your liking.

     In particular, configure which Drupal content types you wish to enable
     relationships for. Enabled content types will have an added "Relations"
     tab, as well as (optionally) a "Related content" fieldset included on
     content creation/editing forms.

  4. Go to [Administer >> User management >> Permissions] to review and
     change the user role permissions for this module. You will typically
     want to grant at least the "create node relations" and "view node
     relations" permissions to the "authenticated user" role.

  5. (See README.txt for information on submitting bug reports.)
