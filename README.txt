
Relations API for Drupal
========================
This Drupal 6.x module provides an API for arbitrary node relationships
based on RDF. Relationships between nodes are stored in a local RDF API
database and can be exported in various RDF formats.

  <http://drupal.org/project/relations>

For more information about the RDF API for Drupal, please refer to:

  <http://drupal.org/project/rdf>


FEATURES FOR USERS
------------------
* Provides an optional "Relations" tab for administrator-specified content
  types, allowing relationship creation between the nodes on a site.


FEATURES FOR DEVELOPERS
-----------------------
* Provides an API for arbitrary node relationships based on RDF.
* Currently supports bidirectional node relationships based on the
  rdfs:seeAlso predicate. This could in the future fairly easily be extended
  to include support for user-defined predicates and for unidirectional
  relationships.
* Relationships are stored in a database-backed RDF API repository, which in
  practice means the {rdf_data_relations} table.
* You can access stored relationships either via the high-level Relations
  API, via any RDF queries (see the rdf_query() function) using the RDF API,
  or directly from the database table.


BUG REPORTS
-----------
Post bug reports and feature requests to the issue tracking system at:

  <http://drupal.org/node/add/project-issue/relations>


FEATURE REQUESTS
----------------
The author is available for contract development and customization relating
to this module. You can reach him at <http://drupal.org/user/26089/contact>.


CREDITS
-------
Developed by Arto Bendiken <http://ar.to/>
Maintained by OpenBand <http://www.openbandlabs.com/>
Sponsored by MakaluMedia Group <http://www.makalumedia.com/>
Developed for SPAWAR <http://www.spawar.navy.mil/>
