<?php

//////////////////////////////////////////////////////////////////////////////
// Relations API settings form

function relations_admin_settings() {
  $form = array();

  // Content type settings
  $form['node_types'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Content type settings'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );
  $form['node_types']['relations_node_types'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Enabled content types'),
    '#default_value' => relations_get_node_types(),
    '#options'       => array_combine(array_keys(node_get_types('names')), array_fill(0, count(node_get_types('names')), '')),
    '#description'   => t('Select the content types for which to enable relationship functionality.'),
  );

  // User interface settings
  $form['ui_settings'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('User interface settings'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );
  $form['ui_settings']['relations_ui_node_tab'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable the "Relations" tab for the content types configured above'),
    '#default_value' => variable_get('relations_ui_node_tab', '1'),
    '#description'   => t('Access to this functionality is subject to the <code>view node relations</code> <a href="@permissions">permission</a>. Note: you may need to clear Drupal\'s menu cache for changes to this setting to take effect.', array('@permissions' => url('admin/user/permissions', array('fragment' => 'module-relations')))),
  );
  $form['ui_settings']['relations_ui_node_edit'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable the embedded "Related content" fieldset on content creation/editing forms'),
    '#default_value' => variable_get('relations_ui_node_edit', '1'),
    '#description'   => t('Access to this functionality is subject to the <code>create node relations</code> <a href="@permissions">permission</a>.', array('@permissions' => url('admin/user/permissions', array('fragment' => 'module-relations')))),
  );

  // Relationship types
  $form['predicates'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Relationship types'),
    '#collapsible'   => TRUE,
    '#collapsed'     => FALSE,
  );
  $form['predicates']['relations_predicate'] = array(
    '#type'          => 'radios',
    '#title'         => t('Default relationship type'),
    '#default_value' => relations_get_predicate(),
    '#options'       => relations_get_predicates('keys'),
    '#description'   => t(''),
  );

  $form['#submit'][] = 'relations_admin_settings_submit';
  return array_merge_recursive(system_settings_form($form), array('#theme' => 'relations_admin_settings', 'buttons' => array('#weight' => 99)));
}

function theme_relations_admin_settings($form) {
  $enabled_types = relations_get_node_types();

  // Content type settings
  $head = array(t('Enabled'), t('Content type'), t('Block title'), array('data' => t('Operations'), 'colspan' => '1'));
  $rows = array();
  foreach (node_get_types('names') as $node_type => $title) {
    $block_title = db_result(db_query("SELECT title FROM {blocks} WHERE module = 'relations' AND delta = '%s' AND theme = '%s'", $node_type, variable_get('theme_default', 'garland')));
    $block_title = !empty($block_title) ? $block_title : t('Relations: !type', array('!type' => $title));
    $rows[] = array(
      drupal_render($form['node_types']['relations_node_types'][$node_type]),
      t($title),
      isset($enabled_types[$node_type]) ? $block_title : '',
      isset($enabled_types[$node_type]) ? l(t('configure block'), 'admin/build/block/configure/relations/'. $node_type) : '',
    );
  }

  $form['node_types']['#value'] = theme('table', $head, $rows, array('class' => 'relations node-types'));
  unset($form['node_types']['relations_node_types']);

  // Relationship types
  $head = array(t('Default'), t('Title'), t('RDF URI'), t('Kind'));
  $rows = array();
  foreach (relations_get_predicates() as $predicate) {
    $rows[] = array(
      drupal_render($form['predicates']['relations_predicate'][$predicate->uri]),
      $predicate->title,
      l($predicate->uri, $predicate->uri),
      !empty($predicate->bidirectional) ? t('Bidirectional') : t('Unidirectional'),
    );
  }

  $form['predicates']['#value'] = theme('table', $head, $rows, array('class' => 'relations predicates'));
  unset($form['predicates']['relations_predicate']);

  return drupal_render($form);
}

function relations_admin_settings_submit($form, &$form_state) {
  extract($form_state['values'], EXTR_SKIP | EXTR_REFS);
  menu_rebuild();
}
